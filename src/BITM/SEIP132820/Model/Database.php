<?php

namespace App\Model;
use PDO;
class Database
{
    public $host="localhost";
    public $dbname="atomic_project_b37";
    public $user="root";
    public $pass="";
    public $DBH;
    public function __construct()
    {
        try
        {
            $this->DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass);
            $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            //echo "connected <br>";
        }
        catch(PDOException $e)
        {
            echo "I'm sorry, Dave. I'm afraid I can't do that.";
        }
    }
}
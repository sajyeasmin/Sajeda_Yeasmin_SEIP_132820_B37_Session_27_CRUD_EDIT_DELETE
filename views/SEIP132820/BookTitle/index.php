<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\book_title\BookTitle;

if(!isset( $_SESSION)) session_start();
echo "<div id=\"message\">". Message::message()."</div>";

$objBookTitle= new BookTitle();
$allData = $objBookTitle->index("obj");
/*foreach($allData as $oneData)
{
   echo $oneData->id."<br>";
   echo $oneData->book_title."<br>";
    echo $oneData->author_name."<br>";
}*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Book Title</h2>
    <div class="table-responsive">
    <table class="table table-bordered">

        <thead>
        <tr>
            <th>Serial No</th>
            <th>Id</th>
            <th>Book Title</th>
            <th>Author Name</th>
            <th>Operation</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $serial=0;
        foreach($allData as $oneData)
        {
        ?>
            <tr >
                <td ><?php echo $serial++; ?></td >
                <td ><?php echo $oneData->id; ?></td >
                <td ><?php echo $oneData->book_title; ?></td >
                <td ><?php echo $oneData->author_name; ?></td >
                <td><a href="edit.php?id=<?php echo $oneData->id; ?>"><button type="button" class="btn btn-success btn-md" name="edit">Edit</button></a>
                <a href="delete.php?id=<?php echo $oneData->id; ?>"><button type="button" class="btn btn-danger btn-md" name="delete">Delete</button></a></td>
            </tr >
        <?php
        }
        ?>
        </tbody>
    </table>
    </div>
</div>

</body>
</html>

<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>
